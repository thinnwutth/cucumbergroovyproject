$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Verify login functionality (\u003chiptest-uid\u003e)",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Automate"
    },
    {
      "name": "@JIRA-YMQA-12"
    },
    {
      "name": "@JIRA-YMQA-65"
    },
    {
      "name": "@JIRA-YMQA-67"
    },
    {
      "name": "@JIRA-YMQA-69"
    },
    {
      "name": "@JIRA-YMQA-70"
    }
  ]
});
formatter.step({
  "name": "launch GitHub web-based application",
  "keyword": "Given "
});
formatter.step({
  "name": "click Sign In from menu",
  "keyword": "When "
});
formatter.step({
  "name": "enter username: \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "enter password: \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "click Sign In",
  "keyword": "And "
});
formatter.step({
  "name": "Login successful",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "hiptest-uid"
      ]
    },
    {
      "cells": [
        "thinwutthmone@yomabank.com",
        "f+1UzUFrqtBsUATFZntgrw\u003d\u003d",
        "uid:80f08033-d84f-4a21-aa15-59f06737618a"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify login functionality (uid:80f08033-d84f-4a21-aa15-59f06737618a)",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Automate"
    },
    {
      "name": "@JIRA-YMQA-12"
    },
    {
      "name": "@JIRA-YMQA-65"
    },
    {
      "name": "@JIRA-YMQA-67"
    },
    {
      "name": "@JIRA-YMQA-69"
    },
    {
      "name": "@JIRA-YMQA-70"
    }
  ]
});
formatter.step({
  "name": "launch GitHub web-based application",
  "keyword": "Given "
});
formatter.match({
  "location": "login.launch_GitHub_webbased_application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Sign In from menu",
  "keyword": "When "
});
formatter.match({
  "location": "login.click_Sign_In_from_menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter username: thinwutthmone@yomabank.com",
  "keyword": "And "
});
formatter.match({
  "location": "login.enter_username(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter password: f+1UzUFrqtBsUATFZntgrw\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "login.enter_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Sign In",
  "keyword": "And "
});
formatter.match({
  "location": "login.click_Sign_In()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Login successful",
  "keyword": "Then "
});
formatter.match({
  "location": "login.Login_successful()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/Logout.feature");
formatter.feature({
  "name": "Logout",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Verify logout functionality",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "launch GitHub web-based application",
  "keyword": "Given "
});
formatter.step({
  "name": "click Sign In from menu",
  "keyword": "When "
});
formatter.step({
  "name": "enter username: \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "enter password: \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "click Sign In",
  "keyword": "And "
});
formatter.step({
  "name": "Login successful",
  "keyword": "Then "
});
formatter.step({
  "name": "click my profile icon",
  "keyword": "When "
});
formatter.step({
  "name": "click sign out button",
  "keyword": "And "
});
formatter.step({
  "name": "Logout successful",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "thinwutthmone@yomabank.com",
        "f+1UzUFrqtBsUATFZntgrw\u003d\u003d"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify logout functionality",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "launch GitHub web-based application",
  "keyword": "Given "
});
formatter.match({
  "location": "login.launch_GitHub_webbased_application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Sign In from menu",
  "keyword": "When "
});
formatter.match({
  "location": "login.click_Sign_In_from_menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter username: thinwutthmone@yomabank.com",
  "keyword": "And "
});
formatter.match({
  "location": "login.enter_username(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter password: f+1UzUFrqtBsUATFZntgrw\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "login.enter_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Sign In",
  "keyword": "And "
});
formatter.match({
  "location": "login.click_Sign_In()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Login successful",
  "keyword": "Then "
});
formatter.match({
  "location": "login.Login_successful()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click my profile icon",
  "keyword": "When "
});
formatter.match({
  "location": "logout.click_my_profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click sign out button",
  "keyword": "And "
});
formatter.match({
  "location": "logout.click_sign_out_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Logout successful",
  "keyword": "Then "
});
formatter.match({
  "location": "logout.Logout_successful()"
});
formatter.result({
  "status": "passed"
});
});