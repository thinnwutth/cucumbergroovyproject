package githubpk

import io.cucumber.datatable.DataTable
import io.cucumber.groovy.EN

this.metaClass.mixin(EN)

Actionwords actionwords = new Actionwords()

Given("launch GitHub web-based application") {  ->
    actionwords.launchGitHubWebbasedApplication()
}

Then("Login successful") { String freeText ->
    actionwords.loginSuccessful(freeText)
}

When("click Sign In") {  ->
    actionwords.clickSignIn()
}

When("click Sign In from menu") {  ->
    actionwords.clickSignInFromMenu()
}

When("enter username: {string}") { String p1 ->
    actionwords.enterUsernameP1(p1)
}

When("enter password: {string}") { String p1 ->
    actionwords.enterPasswordP1(p1)
}

When("click my profile icon") {  ->
    actionwords.clickMyProfileIcon()
}

When("click sign out button") {  ->
    actionwords.clickSignOutButton()
}

Then("Logout successful") {  ->
    actionwords.logoutSuccessful()
}

