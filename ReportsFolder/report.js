$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Verify login functionality (\u003chiptest-uid\u003e)",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Automate"
    },
    {
      "name": "@JIRA-YMQA-12"
    },
    {
      "name": "@JIRA-YMQA-65"
    },
    {
      "name": "@JIRA-YMQA-67"
    },
    {
      "name": "@JIRA-YMQA-69"
    },
    {
      "name": "@JIRA-YMQA-70"
    }
  ]
});
formatter.step({
  "name": "launch GitHub web-based application",
  "keyword": "Given "
});
formatter.step({
  "name": "click Sign In from menu",
  "keyword": "When "
});
formatter.step({
  "name": "enter username: \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "enter password: \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "click Sign In",
  "keyword": "And "
});
formatter.step({
  "name": "Login successful",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "hiptest-uid"
      ]
    },
    {
      "cells": [
        "thinnwutth@gmail.com",
        "f+1UzUFrqtBsUATFZntgrw\u003d\u003d",
        "uid:cbb6de46-c0fe-44f0-b202-31d9a3831b15"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify login functionality (uid:cbb6de46-c0fe-44f0-b202-31d9a3831b15)",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Automate"
    },
    {
      "name": "@JIRA-YMQA-12"
    },
    {
      "name": "@JIRA-YMQA-65"
    },
    {
      "name": "@JIRA-YMQA-67"
    },
    {
      "name": "@JIRA-YMQA-69"
    },
    {
      "name": "@JIRA-YMQA-70"
    }
  ]
});
formatter.step({
  "name": "launch GitHub web-based application",
  "keyword": "Given "
});
formatter.match({
  "location": "github.launch_GitHub_webbased_application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Sign In from menu",
  "keyword": "When "
});
formatter.match({
  "location": "github.click_Sign_In_from_menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter username: thinnwutth@gmail.com",
  "keyword": "And "
});
formatter.match({
  "location": "github.enter_username(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter password: f+1UzUFrqtBsUATFZntgrw\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "github.enter_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Sign In",
  "keyword": "And "
});
formatter.match({
  "location": "github.click_Sign_In()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Login successful",
  "keyword": "Then "
});
formatter.match({
  "location": "github.Login_successful()"
});
formatter.result({
  "status": "passed"
});
});