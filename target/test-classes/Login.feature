Feature: Login


  @Automate @JIRA-YMQA-12 @JIRA-YMQA-65 @JIRA-YMQA-67 @JIRA-YMQA-69 @JIRA-YMQA-70
  Scenario Outline: Verify login functionality (<hiptest-uid>)
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: <username>
    And enter password: <password>
    And click Sign In
    Then Login successful

    Examples:
      | username | password | hiptest-uid |
      | thinwutthmone@yomabank.com | f+1UzUFrqtBsUATFZntgrw== | uid:80f08033-d84f-4a21-aa15-59f06737618a |
      
 
      
      
 
