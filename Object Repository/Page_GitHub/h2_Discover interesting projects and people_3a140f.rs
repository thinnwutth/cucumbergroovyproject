<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Discover interesting projects and people_3a140f</name>
   <tag></tag>
   <elementGuidId>23c112ab-deb8-4266-b34f-0a6eb28e2d43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.h2.lh-condensed.mb-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='panel-1']/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>359228fd-3719-4a6f-8b48-deabee72e546</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h2 lh-condensed mb-2</value>
      <webElementGuid>a339d6c5-a774-43e1-9c3a-ddd6c9bc5843</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Discover interesting projects and people to populate your personal news feed.</value>
      <webElementGuid>1fdef943-261c-4041-a30e-054e0c9add68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;panel-1&quot;)/div[@class=&quot;Box p-5 mt-3&quot;]/h2[@class=&quot;h2 lh-condensed mb-2&quot;]</value>
      <webElementGuid>d3797236-a139-453b-a109-a50878fac10c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='panel-1']/div/h2</value>
      <webElementGuid>8feda633-ee68-438c-bfc2-a3d9a028539e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply changes'])[1]/following::h2[1]</value>
      <webElementGuid>b10336d0-24b4-48a3-8f7c-92c0907c93bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommendations'])[1]/following::h2[1]</value>
      <webElementGuid>3848c7fd-c7d3-49c3-bd4d-de1e95922d8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='watch'])[1]/preceding::h2[1]</value>
      <webElementGuid>62c095ca-d618-4b72-a824-b358958c9e4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='star'])[1]/preceding::h2[1]</value>
      <webElementGuid>ac018c54-fdca-4388-a3ac-ad7cc42afade</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Discover interesting projects and people to populate your personal news feed.']/parent::*</value>
      <webElementGuid>f9fd1f11-ce69-4604-9d9e-56083098f0e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h2</value>
      <webElementGuid>81dd8234-bb36-4f08-aed9-1e673f153f30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Discover interesting projects and people to populate your personal news feed.' or . = 'Discover interesting projects and people to populate your personal news feed.')]</value>
      <webElementGuid>b43528e8-32ea-4a9c-8a41-6f9e318c661b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
