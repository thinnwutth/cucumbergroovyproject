<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_New project_dropdown-caret</name>
   <tag></tag>
   <elementGuidId>b1cc1a90-eb5c-4089-9d39-a247de9ec70f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>details.details-overlay.details-reset.js-feature-preview-indicator-container > summary.Header-link > span.dropdown-caret</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//summary/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c2980525-6b88-4111-8af4-262da3c62b1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-caret</value>
      <webElementGuid>ded244df-b916-4836-9650-c7758447160f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;logged-in env-production page-responsive full-width intent-mouse&quot;]/div[@class=&quot;logged-in env-production page-responsive full-width&quot;]/div[@class=&quot;position-relative js-header-wrapper&quot;]/header[@class=&quot;Header js-details-container Details px-3 px-md-4 px-lg-5 flex-wrap flex-md-nowrap&quot;]/div[@class=&quot;Header-item position-relative mr-0 d-none d-md-flex&quot;]/details[@class=&quot;details-overlay details-reset js-feature-preview-indicator-container&quot;]/summary[@class=&quot;Header-link&quot;]/span[@class=&quot;dropdown-caret&quot;]</value>
      <webElementGuid>19c66c96-78d9-4078-afc1-09606d1f4851</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//summary/span[2]</value>
      <webElementGuid>db2e9dcf-e526-45c9-8c37-5d8385a0eabb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
