<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Lets build fromhere</name>
   <tag></tag>
   <elementGuidId>07a7a46c-89f8-494f-96dd-03343bc17ae4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.h0-mktg.mt-sm-6.mt-md-11.mt-lg-9.mb-2.mb-sm-4.position-relative.z-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up'])[1]/following::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>bcbd29a1-385a-4056-bcfd-e5ff25afa37b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h0-mktg mt-sm-6 mt-md-11 mt-lg-9 mb-2 mb-sm-4 position-relative z-2</value>
      <webElementGuid>55e5cbf5-3e1d-4d2f-9cd1-7c7734c8bb3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          Let’s build from here
        </value>
      <webElementGuid>de712707-4b88-43b0-a346-cb28c3292a1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;logged-out env-production page-responsive header-overlay home-campaign&quot;]/div[@class=&quot;logged-out env-production page-responsive header-overlay home-campaign&quot;]/div[@class=&quot;application-main&quot;]/main[@class=&quot;font-mktg&quot;]/div[@class=&quot;overflow-hidden&quot;]/div[@class=&quot;px-3 home-campaign-hero&quot;]/div[@class=&quot;position-relative pt-3 pt-md-8 pt-lg-12 container-xl js-build-in-trigger build-in-animate&quot;]/div[@class=&quot;d-flex&quot;]/div[@class=&quot;col-11 text-left pt-12 mt-12 pl-2 pl-sm-0&quot;]/h1[@class=&quot;h0-mktg mt-sm-6 mt-md-11 mt-lg-9 mb-2 mb-sm-4 position-relative z-2&quot;]</value>
      <webElementGuid>81bc56a1-1195-4af2-be7d-5b86941e25cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up'])[1]/following::h1[1]</value>
      <webElementGuid>4cc75564-461a-41b1-b481-ed2308d33d75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign in'])[1]/following::h1[1]</value>
      <webElementGuid>a7ac2575-fa80-4b19-9452-bb1df1fab22a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[1]/preceding::h1[1]</value>
      <webElementGuid>715b8ed6-ed05-4376-87be-ec5df5d50d4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for GitHub'])[1]/preceding::h1[1]</value>
      <webElementGuid>dd419abf-4f37-4310-bf7f-0de794d647d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Let’s build from here']/parent::*</value>
      <webElementGuid>42892188-4a0f-434e-8c17-065fb0116bad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>ac7282b5-ce47-4d56-b751-e66074b23cbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
          Let’s build from here
        ' or . = '
          Let’s build from here
        ')]</value>
      <webElementGuid>b8e11a1a-554f-4062-ac05-5315ed6b1915</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
